# frozen_string_literal: true

module MergeRequestCoachHelper
  def select_random_merge_request_coach
    WwwGitLabCom.roulette.each_with_object([]) do |data, memo|
      memo << "@#{data['username']}" if data['merge_request_coach'] && data['available']
    end.sample
  end
end
