module Schedule
  class Variant
    PIPELINE_SCHEDULE_ATTRS = %i[description ref cron cron_timezone active].freeze
    DEFAULT_SCHEDULE_PARAMS = {
      ref: 'master',
      cron_timezone: 'UTC',
      cron: '0 0 * * 1-5', # Monday to Friday
      active: true
    }.freeze

    attr_reader(*PIPELINE_SCHEDULE_ATTRS)

    attr_reader :id, :variables, :project_path

    def initialize(project_path, base_config, variant_config)
      @project_path = project_path
      base_config ||= {}
      variant_config ||= {}

      @id = variant_config[:id]
      @ref = variant_config.fetch(:ref, base_config.fetch(:ref, DEFAULT_SCHEDULE_PARAMS[:ref]))
      @cron = variant_config.fetch(:cron, base_config.fetch(:cron, DEFAULT_SCHEDULE_PARAMS[:cron]))
      @cron_timezone = variant_config.fetch(:cron_timezone, base_config.fetch(:cron_timezone, DEFAULT_SCHEDULE_PARAMS[:cron_timezone]))
      @active = variant_config.fetch(:active, base_config.fetch(:active, DEFAULT_SCHEDULE_PARAMS[:active]))
      @variant_variables = variant_config[:variables] || {}
      @variables = base_config.fetch(:variables, {}).merge(variant_variables)
      @description = variant_config.fetch(:description, default_description)
    end

    def create_params
      PIPELINE_SCHEDULE_ATTRS.each_with_object({}) do |attr, params|
        params[attr] = self.public_send(attr)
      end.sort.to_h
    end

    private

    attr_reader :variant_variables

    def variant_variables_for_default_description
      variant_variables.each_with_object([]) { |(k, v), memo| memo << "'#{k}=#{v}'" }.join(', ')
    end

    def default_description
      description = "[MANAGED] '#{project_path}'"
      description += " (#{variant_variables_for_default_description})" if variant_variables.any?

      description
    end
  end
end
