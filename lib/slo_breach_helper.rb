# frozen_string_literal: true

require 'active_support'
require 'active_support/core_ext/numeric/time'

require_relative 'devops_labels'
require_relative 'group_definition'
require_relative 'versioned_milestone'

module SloBreachHelper
  include DevopsLabels::Context

  SEVERITY_LABELS = {
    s1: 'severity::1',
    s2: 'severity::2',
    s3: 'severity::3',
    s4: 'severity::4'
  }

  SEVERITY_SLO_TARGETS = {
   SEVERITY_LABELS[:s1] => 30,
   SEVERITY_LABELS[:s2] => 60,
   SEVERITY_LABELS[:s3] => 90,
   SEVERITY_LABELS[:s4] => 120
  }

  SLO_WARNING_PERCENTAGE = 75.0

  FRONTEND_LABEL = 'frontend'

  COMMUNITY_BACKUP_DRI = '@matteeyah'

  def current_severity_label
    (label_names & severity_label_names).first
  end

  def slo_breach_looming?
    return false unless severity_label_with_details
    return false if VersionedMilestone.new(self).current?(milestone)

    warning_date = severity_label_added_date + slo_breach_warning_threshold(severity_label_with_details.name).days
    warning_date < Date.today
  end

  def days_til_slo_breach
    "#{days_til_breach} days (#{breach_date.iso8601})"
  end

  def overdue?
    return false unless severity_label_with_details
    return false unless milestone&.due_date

    breach_date < milestone.due_date
  end

  def em_for_team
    if label_names.include?(FRONTEND_LABEL)
      frontend_em_for_team
    else
      backend_em_for_team
    end
  end

  def pm_for_team
    GroupDefinition.pm_for_team(current_group_name)
  end

  def community_team_escalation
    em_for_team || COMMUNITY_BACKUP_DRI
  end

  private

  def severity_label_added_date
    severity_label_with_details.added_at.to_date
  end

  def days_til_breach
    (breach_date - Date.today).to_i
  end

  def breach_date
    severity_label_added_date + slo_target_for_severity
  end

  def slo_target_for_severity
    SEVERITY_SLO_TARGETS[severity_label_with_details.name].days
  end

  def frontend_em_for_team
    GroupDefinition.em_for_team(current_group_name, :frontend)
  end

  def backend_em_for_team
    GroupDefinition.em_for_team(current_group_name)
  end

  def severity_label_with_details
    @severity_label_from_events ||= labels_with_details.find do |label|
      SEVERITY_SLO_TARGETS.key?(label.name)
    end
  end

  def severity_label_names
    SEVERITY_LABELS.values
  end

  def slo_breach_warning_threshold(severity_label)
    slo_target = SEVERITY_SLO_TARGETS[severity_label]
    slo_target * (SLO_WARNING_PERCENTAGE / 100)
  end
end
