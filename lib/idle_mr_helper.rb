# frozen_string_literal: true

module IdleMrHelper
  class << self
    DEFAULT_DAYS = 28

    def idle?(resource, days: DEFAULT_DAYS)
      mr_wip?(resource) &&
        days_since_last_human_update(resource) > days
    end

    def days_since_last_human_update(resource)
      stale_resources_helper(resource).days_since_last_human_update
    end

    private

    def stale_resources_helper(resource)
      StaleResources.new(
        project_path: ENV['TRIAGE_SOURCE_PATH'],
        token: ENV['GITLAB_BOT_AUTOMATED_TRIAGE_TOKEN'],
        project_id: resource[:project_id],
        resource_iid: resource[:iid],
        resource_type: :merge_request
      )
    end

    def mr_wip?(resource)
      resource[:title] !~ /^(draft|wip)/i
    end
  end
end
