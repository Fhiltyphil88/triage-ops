# frozen_string_literal: true

require_relative 'team_member_select_helper'

module ContainerSecurityHelper
  include TeamMemberSelectHelper

  BACKEND_TEAM = "Protect:Container Security BE Team"
  FRONTEND_TEAM = "Protect:Container Security FE Team"

  def container_security_be
    @container_security_be ||= select_random_team_member(BACKEND_TEAM, include_ooo: false)
  end

  def container_security_fe
    @container_security_fe ||= select_random_team_member(FRONTEND_TEAM)
  end
end
