# frozen_string_literal: true

require_relative 'www_gitlab_com'

module TeamMemberSelectHelper

  private

  def select_random_team_member(department, specialty = nil, role = nil, include_ooo: true)
    picked = select_team_members_by_department_specialty_role(department, specialty, role, include_ooo: include_ooo).sample

    picked ||
      # Pick again from the whole pool because we really want someone
      select_team_members_by_department_specialty_role(department, specialty, role, include_ooo: true).sample
  end

  def select_team_members_by_department_specialty_role(department, specialty = nil, role = nil, include_ooo: true)
    select_team_member_usernames(include_ooo: include_ooo) do |data|
      user_matches?(data, department, specialty, role)
    end
  end

  def user_matches?(data, department, specialty, role)
    results = []
    results << (data['departments']&.any? { |dept| dept == department }) if department
    results << (data['specialty']&.include? specialty) if specialty
    results << (data['role'].include? role) if role

    results.all? true
  end

  def select_team_member_usernames(include_ooo: true)
    WwwGitLabCom.team_from_www.each_with_object([]) do |(username, data), memo|
      next if out_of_office?(username) unless include_ooo

      memo << "@#{username}" if yield(data)
    end
  end

  def out_of_office_team_members
    @out_of_office_team_members ||= WwwGitLabCom.roulette
      .filter_map { |data| data['username'] if data.fetch('out_of_office', false) }
  end

  def out_of_office?(username)
    out_of_office_team_members.include?(username)
  end
end
