# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/engineering_allocation_labels_reminder'
require_relative '../../triage/triage/event'

RSpec.describe Triage::EngineeringAllocationLabelsReminder do
  include_context 'with event', 'Triage::IssueEvent' do
    let(:event_attrs) do
      {
        action: 'update',
        label_names: label_names,
        added_label_names: added_label_names,
        from_gitlab_org?: true,
        url: 'https://url'
      }
    end
    let(:label_names) { added_label_names }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["issue.open", "issue.update"]

  describe '#applicable?' do
    context 'when ~"Engineering Allocation" label is added' do
      let(:added_label_names) { ['Engineering Allocation'] }

      include_examples 'event is applicable'
    end

    context 'when other label is added' do
      let(:added_label_names) { ['bug'] }

      include_examples 'event is not applicable'
    end

    context 'when no label is added' do
      include_examples 'event is not applicable'
    end

    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    it 'schedules a EngineeringAllocationLabelsReminderJob 5 minutes later' do
      expect(Triage::EngineeringAllocationLabelsReminderJob).to receive(:perform_in).with(described_class::FIVE_MINUTES, event)

      subject.process
    end
  end
end
