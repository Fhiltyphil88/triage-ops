# frozen_string_literal: true

require_relative '../job/trigger_pipeline_on_approval_job'
require_relative '../triage/processor'
require_relative '../triage/unique_comment'
require_relative 'label_jihu_contribution'

module Triage
  class NewPipelineOnApproval < Processor
    SKIP_WHEN_CHANGES_ONLY_REGEX = %r{\A(?:docs?|qa)/}
    UPDATE_GITALY_BRANCH = 'release-tools/update-gitaly'

    react_to_approvals

    def applicable?
      event.from_gitlab_org? &&
        event.from_gitlab_org_gitlab? &&
        need_new_pipeline?(event) &&
        !merge_request_changes_only?(SKIP_WHEN_CHANGES_ONLY_REGEX) &&
        unique_comment.no_previous_comment?
    end

    def process
      message = unique_comment.wrap(NewPipelineMessage.new(event))

      if event.gitlab_org_author?
        trigger_merge_request_pipeline(message)
      else
        add_discussion(message)
      end
    end

    private

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end

    def merge_request_changes
      @merge_request_changes ||=
        Triage.api_client.
          merge_request_changes(project_id, merge_request_iid).changes
    end

    def project_id
      event.project_id
    end

    def merge_request_iid
      event.iid
    end

    def merge_request_changes_only?(regex)
      paths = %w[old_path new_path]

      merge_request_changes.all? do |change|
        paths.all? do |path|
          change[path].match?(regex)
        end
      end
    end

    def trigger_merge_request_pipeline(message)
      TriggerPipelineOnApprovalJob.perform_async(event.noteable_path, message)
    end

    def need_new_pipeline?(event)
      !(event.source_branch_is?(UPDATE_GITALY_BRANCH) || event.target_branch_is_stable_branch?)
    end

    NewPipelineMessage = Struct.new(:event) do
      def to_s
        <<~MARKDOWN.strip
          #{header}

          #{body}

          #{references}
        MARKDOWN
      end

      def header
        <<~HEADER.chomp
          :wave: `@#{event.event_user_username}`, thanks for approving this merge request.
        HEADER
      end

      def body
        <<~BODY.chomp
          This is the first time the merge request is approved. #{call_to_action}
        BODY
      end

      def references
        <<~REF.chomp
          For more info, please refer to the following links:
          - [`rspec` minimal jobs](https://docs.gitlab.com/ee/development/pipelines.html#rspec-minimal-jobs)
          - [`jest` minimal jobs](https://docs.gitlab.com/ee/development/pipelines.html#jest-minimal-jobs)
          - [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request).
        REF
      end

      def call_to_action
        if event.gitlab_org_author?
          'To ensure full test coverage, a new pipeline has been started.'
        else
          'To ensure full test coverage, please start a new pipeline before merging.'
        end
      end
    end
  end
end
