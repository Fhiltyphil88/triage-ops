# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../job/engineering_allocation_labels_reminder_job'

module Triage
  class EngineeringAllocationLabelsReminder < Processor
    react_to 'issue.open', 'issue.update'

    ENGINEERING_ALLOCATION_LABEL = 'Engineering Allocation'
    FIVE_MINUTES = 300

    def applicable?
      event.from_gitlab_org? &&
        engineering_allocation_label_added?
    end

    def process
      SuckerPunch.logger.info("Scheduling job EngineeringAllocationLabelsReminderJob - Issue URL: #{event.url}")
      EngineeringAllocationLabelsReminderJob.perform_in(FIVE_MINUTES, event)
    end

    private

    def engineering_allocation_label_added?
      event.added_label_names.include?(ENGINEERING_ALLOCATION_LABEL)
    end
  end
end
