# frozen_string_literal: true

require_relative 'appsec_processor'
require_relative '../triage/unique_comment'
require_relative 'label_jihu_contribution'

# TODO: Move this file to triage/processor/appsec/ping_appsec_approval.rb

module Triage
  class PingAppSecOnApproval < AppSecProcessor
    react_to_approvals

    def applicable?
      super &&
        !has_appsec_approval_label? &&
        unique_comment.no_previous_comment?
    end

    def process
      message = unique_comment.wrap(PingAppSecMessage.new(event))

      add_discussion(message)
    end

    private

    PingAppSecMessage = Struct.new(:event) do
      def to_s
        <<~MARKDOWN.strip
          #{header}

          #{body}

          #{request_appsec_review}
        MARKDOWN
      end

      def header
        <<~HEADER.chomp
          :wave: `@#{event.event_actor_username}`, thanks for approving this merge request.
        HEADER
      end

      def body
        <<~BODY.chomp
          This is the first time the merge request is approved. Please wait for AppSec approval.
        BODY
      end

      def request_appsec_review
        <<~MARKDOWN.chomp
          cc @#{Triage::GITLAB_COM_APPSEC_GROUP} this is a ~"#{Triage::LabelJiHuContribution::LABEL_NAME}", please follow the [JiHu contribution review process](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/jihu-security-review-process.html#security-review-workflow-for-jihu-contributions).
        MARKDOWN
      end
    end
  end
end
