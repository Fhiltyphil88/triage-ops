# frozen_string_literal: true

require 'sentry-raven'

Raven.configure do |config|
  if ENV['SENTRY_DSN']
    config.dsn = ENV['SENTRY_DSN']
  else
    config.logger = Logger.new(File.open(IO::NULL, 'w'))
  end
end
